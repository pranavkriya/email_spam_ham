#!/usr/bin/python3

import numpy as np

from flask import Flask,render_template,request,redirect,url_for
from multiple_mails import return_fn
from body_mail import return_body
from connection import connection_check
from send_mail import send1
import json

app = Flask(__name__)

@app.route('/')
def home():
	return render_template('homepage.html')

@app.route('/maillist',methods=['POST','GET'])
def mail_list():
	ans = connection_check()
	if ans==1:
		results = return_fn()
		return render_template('mymail.html',results=results)
	else :
		return 'ERROR'



@app.route('/show<int:uid>')
def show_body(uid):
	content1 = return_body(uid)
	return content1  #render_template('show_mail.html',content=content1)

@app.route('/sendmail',methods=['POST','GET'])
def send2():
	send1()
	data="done"
	results = return_fn()
	return render_template('mymail.html',results=results,abcd=json.dumps(data))
	


if __name__ == '__main__':
	app.run(host='192.168.43.168',debug = True)  # change the host_id according to users ip


